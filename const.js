'use strict';

import Dotenv from 'dotenv';

Dotenv.load();

const APP_URL = process.env.APP_URL;;
const BUILD_PATH = './public/';
const PUBLIC_MANIFEST = BUILD_PATH + 'rev-manifest.json';
const SRC_DIR = './src/';
const PUBLIC_HTML = SRC_DIR + 'index.html';
const PUBLIC_JS = SRC_DIR + 'assets/js/main.js';
const PUBLIC_CSS = SRC_DIR + 'assets/css/style.css';


export {
  APP_URL,
  BUILD_PATH,
  PUBLIC_MANIFEST,
  SRC_DIR,
  PUBLIC_HTML,
  PUBLIC_JS,
  PUBLIC_CSS
};
