'use strict';

import gulp from 'gulp';
import gulpRev from 'gulp-rev-all';
import { BUILD_PATH, PUBLIC_HTML2, PUBLIC_JS2 } from './const';

gulp.task('start-revision', () => {
  let revFiles = gulp.src([ PUBLIC_JS2 ]);
  return revFiles
    .pipe(gulpRev.revision({ includeFilesInManifest: ['.css', '.js', '.ico', '.png'] }))
    .pipe(gulp.dest(BUILD_PATH))
    .pipe(gulpRev.manifestFile())
    .pipe(gulp.dest(BUILD_PATH));
});
