'use strict';

import gulp from 'gulp';
import gulpRev from 'gulp-rev-all';
import gulpRevReplace from 'gulp-rev-replace';
import { BUILD_PATH, PUBLIC_HTML, PUBLIC_JS, PUBLIC_MANIFEST, APP_URL, PUBLIC_CSS } from './const';

gulp.task('start-revision', () => {
  let revFiles = gulp.src([ PUBLIC_JS, PUBLIC_CSS ]);
  return revFiles
    .pipe(gulpRev.revision({ includeFilesInManifest: ['.css', '.js', '.ico', '.png'] }))
    .pipe(gulp.dest(BUILD_PATH))
    .pipe(gulpRev.manifestFile())
    .pipe(gulp.dest(BUILD_PATH));
});

gulp.task('finish-revision', () => {
  let manifest = gulp.src(PUBLIC_MANIFEST);
  return gulp.src(PUBLIC_HTML)
    .pipe(gulpRevReplace({ manifest: manifest, prefix: APP_URL }))
    .pipe(gulp.dest(BUILD_PATH));
});